/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.lang;

/**
 * Created 8/23/21 2:37 AM
 */
public class FunctionBodyEmpty extends FunctionBody {
	@Override
	public void setAbstract(boolean aAbstract) {

	}

	@Override
	public boolean getAbstract() {
		return true;
	}
}

//
//
//
